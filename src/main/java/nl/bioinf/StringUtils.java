package nl.bioinf;

/**
 * Creation date: Dec 19, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class StringUtils {
    /**
     * concatenates an array of strings into a single character array
     * @param words
     * @return
     */
    public static char[] stringArrayToUppercaseCharacterArray(String[] words) {
        return null;
    }

    /**
     * finds and returns a wordt with the given substring. Does so case insensitive.
     *
     * @param words
     * @param substringToFind
     * @return word
     * @throws IllegalArgumentException when no match found
     */
    public static String findWordWithSubstring(String[] words, String substringToFind) {
        return null;
    }
}
