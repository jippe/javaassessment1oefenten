package nl.bioinf;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import org.junit.FixMethodOrder;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Creation date: Dec 19, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssessmentTest {

    private static final double DELTA = 1E-5;
    private static Properties testAnswers;
    private static int maximumPoints = 0;

    private static Map<String, Integer> answerPoints = new HashMap<>();

    static {
        answerPoints.put("Assignment 1a", 0);
        answerPoints.put("Assignment 1b", 0);
        answerPoints.put("Assignment 1c", 0);
        answerPoints.put("Assignment 1d", 0);
        answerPoints.put("Assignment 1e", 0);
        answerPoints.put("Assignment 1f", 0);

        answerPoints.put("Assignment 2a", 0);
        answerPoints.put("Assignment 2b", 0);
        answerPoints.put("Assignment 2c", 0);
        answerPoints.put("Assignment 2d", 0);
        answerPoints.put("Assignment 2e", 0);

        answerPoints.put("Assignment 3", 0);
        answerPoints.put("Assignment 4", 0);
        answerPoints.put("Assignment 5", 0);
        answerPoints.put("Assignment 6", 0);
        answerPoints.put("Assignment 7", 0);
    }

    @BeforeClass
    public static void loadProperties() throws Exception {
        testAnswers = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("answers/question_answers.properties");
            testAnswers.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

//    @Test
//    public void testAssignment1a() throws Exception {
//        System.out.print("Assignment1a..");
//        int points = 2;
//        maximumPoints += points;
//        assertEquals("String", testAnswers.getProperty("Assignment1.a"));
//        answerPoints.put("Assignment 1a", points);
//        System.out.println("..OK");
//    }

    @Test
    public void testAssignment1b() throws Exception {
        System.out.print("Assignment1b..");
        int points = 2;
        maximumPoints += points;
        String answer = testAnswers.getProperty("Assignment1.b");
        if (answer.equals("long")) {
            answerPoints.put("Assignment 1b", points);
        } else if (answer.equals("int")) {
            answerPoints.put("Assignment 1b", 1);
        } else {
            fail("Not a valid type: " + answer);
        }
        System.out.println("..OK");
    }

    @Test
    public void testAssignment1c() throws Exception {
        System.out.print("Assignment1c..");
        int points = 2;
        maximumPoints += points;
        assertEquals("boolean", testAnswers.getProperty("Assignment1.c"));
        answerPoints.put("Assignment 1c", points);
        System.out.println("..OK");
    }

    @Test
    public void testAssignment1d() throws Exception {
        System.out.print("Assignment1d..");
        int points = 2;
        maximumPoints += points;
        assertEquals("enum", testAnswers.getProperty("Assignment1.d"));
        answerPoints.put("Assignment 1d", points);
        System.out.println("..OK");
    }
    @Test
    public void testAssignment1e() throws Exception {
        System.out.print("Assignment1e..");
        int points = 2;
        maximumPoints += points;
        assertEquals("double", testAnswers.getProperty("Assignment1.e"));
        answerPoints.put("Assignment 1e", points);
        System.out.println("..OK");
    }
    @Test
    public void testAssignment1f() throws Exception {
        System.out.print("Assignment1f..");
        int points = 2;
        maximumPoints += points;
        assertEquals("short", testAnswers.getProperty("Assignment1.f"));
        answerPoints.put("Assignment 1f", points);
        System.out.println("..OK");
    }



    @Test
    public void testAssignment2a() throws Exception {
        System.out.print("Assignment2a..");
        int points = 2;
        maximumPoints += points;
        assertEquals("private", testAnswers.getProperty("Assignment2.a"));
        answerPoints.put("Assignment 2a", points);
        System.out.println("..OK");
    }

    @Test
    public void testAssignment2b() throws Exception {
        System.out.print("Assignment2b..");
        int points = 2;
        maximumPoints += points;
        assertEquals("static", testAnswers.getProperty("Assignment2.b"));
        answerPoints.put("Assignment 2b", points);
        System.out.println("..OK");
    }

    @Test
    public void testAssignment2c() throws Exception {
        System.out.print("Assignment2c..");
        int points = 2;
        maximumPoints += points;
        assertEquals("protected", testAnswers.getProperty("Assignment2.c"));
        answerPoints.put("Assignment 2c", points);
        System.out.println("..OK");
    }

    @Test
    public void testAssignment2d() throws Exception {
        System.out.print("Assignment2d..");
        int points = 2;
        maximumPoints += points;
        assertEquals("final", testAnswers.getProperty("Assignment2.d"));
        answerPoints.put("Assignment 2d", points);
        System.out.println("..OK");
    }

    @Test
    public void testAssignment2e() throws Exception {
        System.out.print("Assignment2e..");
        int points = 2;
        maximumPoints += points;
        assertEquals("default", testAnswers.getProperty("Assignment2.e"));
        answerPoints.put("Assignment 2e", points);
        System.out.println("..OK");
    }

    @Test
    public void testAssignment3() throws Exception {
        System.out.print("\nAssignment3..");
        int points = 10;
        maximumPoints += points;
        String[] input = {"Hello", "World"};
        char[] observed = StringUtils.stringArrayToUppercaseCharacterArray(input);
        char[] expected = {'H', 'E', 'L', 'L', 'O', 'W', 'O', 'R', 'L', 'D'};
        assertArrayEquals(expected, observed);

        input = new String[]{"Java", "rocks!"};
        observed = StringUtils.stringArrayToUppercaseCharacterArray(input);
        expected = new char[]{'J', 'A', 'V', 'A', 'R', 'O', 'C', 'K', 'S', '!'};
        assertArrayEquals(expected, observed);
        System.out.println("..OK");
        answerPoints.put("Assignment 3", points);
    }

    @Test
    public void testAssignment4a() throws Exception {
        System.out.print("\nAssignment4a..");
        int points = 5;
        maximumPoints += points;
        String[] input = {"Hello", "World", "Java", "rocks!"};
        String expected = "rocks!";
        String search = "Ock";
        String observed = StringUtils.findWordWithSubstring(input, search);
        assertEquals(expected, observed);
        System.out.println("..OK");
        answerPoints.put("Assignment 4", points);
    }

    @Test
    public void testAssignment4b() throws Exception {
        System.out.print("Assignment4b..");
        int points = 5;
        maximumPoints += points;
        String[] input = {"Hello", "World", "Java", "rocks!"};
        String search = "Oock";
        try {
            StringUtils.findWordWithSubstring(input, search);
        } catch (IllegalArgumentException ex) {
            System.out.println("..OK");
            answerPoints.put("Assignment 4", answerPoints.get("Assignment 4") + points);
        }
    }

    @Test
    public void testAssignment5() throws Exception {
        System.out.print("\nAssignment5..");
        int points = 10;
        maximumPoints += points;

        String inputFile = "data/sequencer_data.txt";
        Sequencer sequencer = new Sequencer();
        List<PositionIntensities> positionIntensities = sequencer.readIntensities(inputFile);

        //check whether it was implemented
        assertNotNull(positionIntensities);
        System.out.print("was implemented");

        assertEquals(12, positionIntensities.get(11).getPosition());
        assertEquals("if Actual is 11 then this is backupData!", 12, positionIntensities.get(11).getScoreG());

        assertEquals(15, positionIntensities.get(14).getPosition());
        assertEquals("if Actual is 11 then this is backupData!", 17, positionIntensities.get(14).getScoreT()); //is 11 in copied data

        assertEquals(23, positionIntensities.get(22).getPosition());
        assertEquals("if Actual is 20 then this is backupData!", 21, positionIntensities.get(22).getScoreA()); //is 20 in copied data

        assertEquals(26, positionIntensities.get(25).getPosition());
        assertEquals("if Actual is 14 then this is backupData!", 13, positionIntensities.get(25).getScoreC()); //is 14 in copied data

        System.out.print("..and not falsely taken from fetchBackupData..");
        System.out.println("..OK");
        answerPoints.put("Assignment 5", points);
    }

    @Test
    public void testAssignment6() throws Exception {
        System.out.print("\nAssignment6a..");
        int points = 5;
        maximumPoints += points;

        PositionIntensities pi = new PositionIntensities(1, 50, 25, 25, 0);
        Basecall observed = pi.getBasecall();
        Basecall expected = new Basecall('N', 0.5);
        assertEquals(expected.nucleotide, observed.nucleotide);
        assertEquals(expected.probability, observed.probability, DELTA);

        pi = new PositionIntensities(1, 10, 80, 5, 5);
        observed = pi.getBasecall();
        expected = new Basecall('A', 0.8);
        assertEquals(expected.nucleotide, observed.nucleotide);
        assertEquals(expected.probability, observed.probability, DELTA);
        System.out.println("..OK");
        answerPoints.put("Assignment 6", answerPoints.get("Assignment 6") + points);
    }

    @Test
    public void testAssignment6b() throws Exception {
        System.out.print("Assignment6b..");
        int points = 5;
        maximumPoints += points;

        Sequencer sequencer = new Sequencer();
        List<PositionIntensities> positionIntensities = sequencer.fetchBackupData();
        List<Basecall> basecalls = sequencer.executeBasecalls(positionIntensities);
        assertEquals( "expecting a list of 30 basecalls", 30, basecalls.size());
        assertEquals(0.7876106, basecalls.get(0).probability, DELTA);
        System.out.println("..OK");
        answerPoints.put("Assignment 6", answerPoints.get("Assignment 6") + points);
    }

    @Test
    public void testAssignment7() throws Exception {
        System.out.print("\nAssignment7..");
        int points = 10;
        maximumPoints += points;
        Sequencer sequencer = new Sequencer();
        List<Basecall> basecalls = createBaseCalls();
        RunQuality observed = sequencer.determineRunQuality(basecalls);

        assertEquals(0.5, observed.getAverageProbability(), DELTA);
        assertEquals(1, observed.getNumberOfAmbiguities());
        System.out.println("..OK");
        answerPoints.put("Assignment 7", points);
    }

    private List<Basecall> createBaseCalls() {
        List<Basecall> list = new ArrayList<>();
        list.add(new Basecall('G', 0.5));
        list.add(new Basecall('A', 0.6));
        list.add(new Basecall('N', 0.4));
        list.add(new Basecall('T', 0.5));
        list.add(new Basecall('C', 0.7));
        list.add(new Basecall('C', 0.3));

        return list;
    }

    @Test
    public void testAssignment999() throws Exception {
        System.out.println("\nAssignment End..");
        List<String> keys = new ArrayList<>(answerPoints.keySet());
        Collections.sort(keys);
        int total = 0;
        for(String key : keys) {
            total += answerPoints.get(key);
            System.out.println(key + ": " + answerPoints.get(key) + " points");
        }
        System.out.println("Assignment total points = " + total);
        double grade = (total/maximumPoints * 10) < 1 ? 1 : (total/maximumPoints * 10);
        System.out.printf("Grade = %.2f", grade);
    }

}